const isWinner = (squares) => {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[b] === squares[c]) {
      
      return { 
        winner: squares[a], 
        lines: lines[i].join() };
    }
  }
  return {winner:null};
};

const setLine=(lines)=>{
  if(lines==='0,1,2'){
    return [{top:80}, {left:null},{className: 'line-horizontal'}]
  }else if(lines==='3,4,5'){
    return [{top:230}, {left:null}, {className: 'line-horizontal'}]
  }else if(lines==='6,7,8'){
    return [{top:380}, {left:null}, {className:'line-horizontal'}]
  }else if(lines==='0,3,6'){
    return [{top: null}, {left:80}, {className: 'line-vertical'}]
  }else if(lines==='1,4,7'){
    return [{top: null}, {left:230}, {className: 'line-vertical'}]
  }else if(lines==='2,5,8'){
    return [{top: null}, {left:380}, {className: 'line-vertical'}]
  }else if(lines==='0,4,8'){
    return [{top: null}, {left:null}, {className: 'line-oblique-left'}]
  }else if(lines==='2,4,6'){
    return [{top: null}, {left:null}, {className: 'line-oblique-right'}]
  }
}

export { isWinner, setLine };
