import React from 'react';
import './Modal.scss'

function Modal(props) {
  const inputChange = (e) => {
    props.func(e.target.value, e.target.id);
  };
  return (
    <div className={props.className}>
      <div>
        <h2>{props.text}</h2>
        <input id='firstPlayer' placeholder='X player name' onChange={inputChange} ></input>
        <input id='secondPlayer' placeholder='O player name' onChange={inputChange}></input>
        <button onClick={props.closeModal}>Let's go play!</button>
        </div>
    </div>
  );
}

export default Modal;